package com.projetCICD.takeAndPutYourOwn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TakeAndPutYourOwnApplication {

	public static void main(String[] args) {
		SpringApplication.run(TakeAndPutYourOwnApplication.class, args);
	}

}
