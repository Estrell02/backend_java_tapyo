package com.projetCICD.takeAndPutYourOwn.mapper;

import com.projetCICD.takeAndPutYourOwn.model.dto.FileDto;
import com.projetCICD.takeAndPutYourOwn.model.entities.File;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring",nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, nullValueCheckStrategy =  NullValueCheckStrategy.ALWAYS)
public interface FileMapper {
    File toEntity(FileDto fileDto);

    FileDto toDto(File file);

    void copy(FileDto fileDto, @MappingTarget File file);
}