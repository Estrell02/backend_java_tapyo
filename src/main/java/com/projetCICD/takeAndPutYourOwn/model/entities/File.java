package com.projetCICD.takeAndPutYourOwn.model.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Table(name = "files", indexes = {
        @Index(name = "name", columnList = "name", unique = true)
})
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class File {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "data", nullable = false)
    @Lob
    private byte[] data;

    public File(String name, String type, byte[]data){
        this.name = name;
        this.type = type;
        this.data = data;
    }
}