package com.projetCICD.takeAndPutYourOwn.presentation.api;

import com.projetCICD.takeAndPutYourOwn.message.ResponseFile;
import com.projetCICD.takeAndPutYourOwn.message.ResponseMessage;
import com.projetCICD.takeAndPutYourOwn.model.dto.FileDto;
import com.projetCICD.takeAndPutYourOwn.service.IFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/takeandput/file")
@Slf4j
public class FileRestController {

    @Autowired
    IFile iFile;

    @GetMapping("/list")
    public ResponseEntity<List<ResponseFile>> getAllFiles(){
        FileRestController.log.info("Liste de tous les fichiers");
        List<ResponseFile> files = iFile.list().stream().map(dbFile -> {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/files/")
                    .path(dbFile.getId())
                    .toUriString();
            return new ResponseFile(
                    dbFile.getName(),
                    fileDownloadUri,
                    dbFile.getType(),
                    dbFile.getData().length);
        }).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @PostMapping(value = "/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file){
        FileRestController.log.info("Téléversement d'un fichier");
        String message = "";
        try {
            iFile.store(file);
            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }

    @GetMapping("/{id}/download")
    public ResponseEntity<byte[]> downloadFile(@PathVariable String id){
        FileRestController.log.info("Téléchargement du fichier : " + id);
        FileDto fileDto = iFile.getFileById(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDto.getName() + "\"")
                .body(fileDto.getData());
    }

}
