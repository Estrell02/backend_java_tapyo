package com.projetCICD.takeAndPutYourOwn.repository;

import com.projetCICD.takeAndPutYourOwn.model.entities.File;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<File, String> {
}
