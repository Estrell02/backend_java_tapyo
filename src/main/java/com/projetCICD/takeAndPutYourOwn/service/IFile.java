package com.projetCICD.takeAndPutYourOwn.service;

import com.projetCICD.takeAndPutYourOwn.model.dto.FileDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IFile {
    List<FileDto> list();
    FileDto store(MultipartFile file) throws IOException;
    FileDto getFileById(String id);
}
