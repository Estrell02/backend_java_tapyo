package com.projetCICD.takeAndPutYourOwn.serviceImpl;

import com.projetCICD.takeAndPutYourOwn.mapper.FileMapper;
import com.projetCICD.takeAndPutYourOwn.model.dto.FileDto;
import com.projetCICD.takeAndPutYourOwn.model.entities.File;
import com.projetCICD.takeAndPutYourOwn.repository.FileRepository;
import com.projetCICD.takeAndPutYourOwn.service.IFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileImpl implements IFile {

    @Autowired
    FileMapper fileMapper;

    @Autowired
    FileRepository fileRepository;

    @Override
    public List<FileDto> list() {
        return fileRepository.findAll().stream()
                .map(fileMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public FileDto store(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        File fileDB = new File(fileName, file.getContentType(), file.getBytes());
        return fileMapper.toDto(fileRepository.save(fileDB));
    }

    @Override
    public FileDto getFileById(String id) {
        return fileMapper.toDto(fileRepository.findById(id).get());
    }
}
